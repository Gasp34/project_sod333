# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 15:41:10 2020

@author: Gaspard
"""
import numpy as np # calcul numerique
import numpy.random as rnd # fonctions pseudo-aleatoires
import matplotlib.pyplot as plt # fonctions graphiques a la MATLAB
import matplotlib.animation as anim # fonctions d’animation
import scipy.io as io # fonctions pour l’ouverture des fichiers .mat de MATLAB
import math
import scipy

np.random.seed(10)

map = io.loadmat('mnt.mat')['map']
N1 = map.shape[1]
N2 = map.shape[0]

(X1_min, X1_max)=(-10000, 10000)
(X2_min, X2_max)=(-10000 , 10000)
r0=(-6000, 2000)
v0=(+120, 0)
sigma_r0=100
sigma_v0=10
sigma_INS=7
sigma_ALT=10
sigma_BAR=20
delta=1
T=101

plt.imshow(map,cmap='jet',extent=[X1_min,X1_max,X2_min,X2_max])

traj = io.loadmat('traj.mat')
rtrue = traj['rtrue']
vtrue = traj['vtrue']

plt.imshow(map,cmap='jet',extent=[X1_min,X1_max,X2_min,X2_max])
plt.plot(rtrue[0,:],rtrue[1,:],'r-')

a_INS = io.loadmat('ins.mat')['a_INS']
r_INS = np.zeros(rtrue.shape)
v_INS = np.zeros(vtrue.shape)
r_INS[:,0] = r0
v_INS[:,0] = v0

nmax = a_INS.shape[1]

for k in range(1,nmax):
    r_INS[:,k] = r_INS[:,k-1]+delta*v_INS[:,k-1]
    v_INS[:,k] = v_INS[:,k-1]+delta*a_INS[:,k-1]
  
plt.imshow(map,cmap='jet',extent=[X1_min,X1_max,X2_min,X2_max])
plt.plot(rtrue[0,:],rtrue[1,:],'r-')
plt.plot(r_INS[0,:],r_INS[1,:],'m-')
    
# eval_h renvoie la hauteur du relief au point r si les coordonnées de r ne sortent pas de la carte
def eval_h(r):
    x1 = (r[0] - X1_min)/(X1_max-X1_min)
    x2 = (r[1] - X2_min)/(X2_max-X2_min)
    i = int((N2-1)*(1 - x2))
    j = int((N1-1)*x1)
    try:
        h = map[i,j]
        return h
    except:
        return False

def resampling_multi(w, N):
    u_tild = np.zeros((N))
    alpha = np.zeros((N))
    u_ord = np.zeros((N))
    s = np.zeros((N))
#
    w = w / w.sum()
    s = np.cumsum(w)
    u_tild = rnd.uniform(0,1, N)
#
    for i in range(N):
        alpha[i] = u_tild[i] ** (1 / float(i + 1))
    alpha = np.cumprod(alpha)
    u_ord = alpha[N-1] / alpha
    u = np.append(u_ord, float ("inf"))
#
    ancetre = np.zeros(N, dtype = int)
    descendants = np.zeros(N, dtype = int)
    i = 0
    for j in range(N):
        o = 0
        while u[i] <= s[j]:
            ancetre [i] = j
            i = i + 1
            o = o + 1
        descendants[j] = o
    return ancetre

h = np.zeros(rtrue.shape[1])
for t in range(len(h)):
    h[t] = eval_h(rtrue[:,t])

# update met a jour les delta rk,vk selon l'équation d'état
def update(X):
    A = [[1,0,1,0],[0,1,0,1],[0,0,1,0],[0,0,0,1]]
    B = np.zeros((4,1))
    B[2:] = rnd.normal(0,sigma_INS,(2,1))
    Y = np.matmul(A,X)-B
    return Y
    
h_ALT = io.loadmat('alt.mat')['h_ALT'][0]

sigma = math.sqrt(sigma_ALT**2 + sigma_BAR**2)
f = lambda x : 1/(math.sqrt(2*math.pi*pow(sigma,2))) * math.exp(-pow(x,2)/(2*pow(sigma,2)))

d = np.zeros((4,T)) # delta rk et delta vk
d[:2,0] = rnd.normal(0,sigma_r0,2) #on itialise delta r0 et v0
d[2:,0] = rnd.normal(0,sigma_v0,2) 

N = 1000  #nombre de particules

#on créé N particules initialisé selon la même condition initiale de delta r0 et v0
E0 = np.zeros((4,N))
E0[:2,:] = rnd.normal(0,sigma_r0,(2,N))
E0[2:,:] = rnd.normal(0,sigma_v0,(2,N))

#on calcule les poids initiaux
w = np.zeros((N))
for n in range(N):
    diff = h_ALT[0] - eval_h(r_INS[:,0]+E0[:2,n])
    w[n] = f(diff)
w = w/w.sum()

Ek = np.zeros((4,N)) #particules à l'instant k
Ek_1 = E0 #particules a l'instant k-1

r = np.zeros((2,N)) #sera utilisé pour stocker les positions des particules

SIR = False
SIS = True
Adap = False
#SIR
if SIR:
    for t in range(1,T):
        indices = resampling_multi(w, N) #re-echantillonnage
        for i,j in enumerate(indices): #prédiction
            Ek[:,i:i+1] = update(Ek_1[:,j:j+1])    
        
        for n in range(N): #correction
            diff = h_ALT[t] - eval_h(r_INS[:,t]+Ek[:2,n])
            w[n] = f(diff)
        w = w/w.sum()
            
        d[:,t] = np.sum([Ek[:,n]*w[n] for n in range(N)],axis=0) #moyenne pondérée du nuage
        r_mean = (r_INS + d[:2,:])[:,t]
        
        Ek_1 = np.copy(Ek)
        
        for n in range(N): #nuage de particule
            r[:,n] = r_INS[:,t]+Ek[:2,n]
        
        #on affiche le nuage en noir, rk en bleu, r_INS en vert et la moyenne du nuage en rouge
        plt.imshow(map,cmap='jet',extent=[X1_min,X1_max,X2_min,X2_max])
        plt.plot(r[0],r[1],marker="+",color="black",linestyle='None')
        plt.plot(rtrue[0][t],rtrue[1][t],marker="+",color="blue",linestyle='None')
        plt.plot(r_mean[0],r_mean[1],marker="+",color="red",linestyle='None')
        plt.plot(r_INS[0][t],r_INS[1][t],marker="+",color="green",linestyle='None')
        plt.show()
        
#SIS

if SIS:
    for t in range(1,T):
        for i in range(N): #prediction
            Ek[:,i:i+1] = update(Ek_1[:,i:i+1])    
        
        for n in range(N): #correction
            h = eval_h(r_INS[:,t]+Ek[:2,n])
            if h:
                diff = h_ALT[t] - h
                w[n] = f(diff)*w[n]
            else: #on met un poids nul aux particules qui sortent de la carte
                w[n]=0
        w = w/w.sum()
            
        d[:,t] = np.sum([Ek[:,n]*w[n] for n in range(N)],axis=0) #moyenne pondérée du nuage
        r_mean = (r_INS + d[:2,:])[:,t]
        
        Ek_1 = np.copy(Ek)
        
        for n in range(N): #nuage de particule
            r[:,n] = r_INS[:,t]+Ek[:2,n]
            
        #on affiche le nuage en noir, rk en bleu, r_INS en vert et la moyenne du nuage en rouge        
        plt.imshow(map,cmap='jet',extent=[X1_min,X1_max,X2_min,X2_max])
        plt.plot(r[0][w>0],r[1][w>0],marker="+",color="black",linestyle='None')
        plt.plot(rtrue[0][t],rtrue[1][t],marker="+",color="blue",linestyle='None')
        plt.plot(r_mean[0],r_mean[1],marker="+",color="red",linestyle='None')
        plt.plot(r_INS[0][t],r_INS[1][t],marker="+",color="green",linestyle='None')
        plt.show()

#SIS
c = 0.1
if Adap:
    for t in range(1,T):
        Neff = 1/sum(w**2)
        
        #re-echantillonnage si Neff <= cN + prediction ensuite
        if Neff > c*N:
            for i in range(N):
                Ek[:,i:i+1] = update(Ek_1[:,i:i+1])
        else:
            indices = resampling_multi(w, N)
            for i,j in enumerate(indices):
                Ek[:,i:i+1] = update(Ek_1[:,j:j+1])
                    
        for n in range(N): #correction (selon si on a re-echantillonne)
            h = eval_h(r_INS[:,t]+Ek[:2,n])
            if h:
                diff = h_ALT[t] - h
                if Neff > c*N:
                    w[n] = f(diff)*w[n]
                else:
                    w[n] = f(diff)
            else:
                w[n]=0
        w = w/w.sum()
            
        d[:,t] = np.sum([Ek[:,n]*w[n] for n in range(N)],axis=0) #moyenne pondérée du nuage
        r_mean = (r_INS + d[:2,:])[:,t]
        
        Ek_1 = np.copy(Ek)
        
        for n in range(N): #nuage de particule
            r[:,n] = r_INS[:,t]+Ek[:2,n]
        
        #on affiche le nuage en noir, rk en bleu, r_INS en vert et la moyenne du nuage en rouge        
        plt.imshow(map,cmap='jet',extent=[X1_min,X1_max,X2_min,X2_max])
        plt.plot(r[0][w>0],r[1][w>0],marker="+",color="black",linestyle='None')
        plt.plot(rtrue[0][t],rtrue[1][t],marker="+",color="blue",linestyle='None')
        plt.plot(r_mean[0],r_mean[1],marker="+",color="red",linestyle='None')
        plt.plot(r_INS[0][t],r_INS[1][t],marker="+",color="green",linestyle='None')
        plt.show()